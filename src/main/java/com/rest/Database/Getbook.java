package com.rest.Database;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by pavan on 1/3/16.
 */
public class Getbook {
    public static JSONObject getBook() {
        DatabaseConnection db= new DatabaseConnection();
        JSONObject res = null;
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        try {
            String sql1 = "SELECT * FROM Tbl_book_master";
            System.out.println(sql1);
            stmt = conn.prepareStatement(sql1);
            //stmt.setString(1, "%"+query+"%");
            ResultSet rs1 = stmt.executeQuery();
            JSONArray bookArray = new JSONArray();
            while(rs1.next()){
                JSONObject tmp=new JSONObject();
                tmp.put("_id", rs1.getString("_id"));
                tmp.put("name", rs1.getString("fld_book_name"));
                bookArray.put(tmp);
            }
            res = new JSONObject();
            res.put("success", true);
            res.put("message", "successfully search");
            res.put("data", bookArray);
            stmt.close();
        } catch (Exception e1) {
            e1.printStackTrace();
            res = new JSONObject();
            res.put("success", false);
            res.put("message", " something went wrong");
        }
        return res;
    }
    public static JSONObject getBookById(String  id) {
        DatabaseConnection db= new DatabaseConnection();
        JSONObject res = null;
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        try {
            String sql1 = "SELECT * FROM Tbl_book_chapter where fld_book_id=?";
            System.out.println(sql1);
            stmt = conn.prepareStatement(sql1);
            stmt.setString(1, id);
            ResultSet rs1 = stmt.executeQuery();
            JSONArray bookArray = new JSONArray();
            while(rs1.next()){
                JSONObject tmp=new JSONObject();
                tmp.put("chapterId", rs1.getString("fld_chapter_id"));
                tmp.put("chapterNum", rs1.getString("fld_chapter_Number"));
                tmp.put("chapterName", rs1.getString("fld_chapter_name"));
                bookArray.put(tmp);
            }
            res = new JSONObject();
            res.put("success", true);
            res.put("message", "featch chapter successfully");
            res.put("data", bookArray);
            stmt.close();
        } catch (Exception e1) {
            e1.printStackTrace();
            res = new JSONObject();
            res.put("success", false);
            res.put("message", " something went wrong");
        }
        return res;
    }
}
