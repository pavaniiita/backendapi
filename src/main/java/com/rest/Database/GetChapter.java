package com.rest.Database;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by pavan on 1/3/16.
 */
public class GetChapter {

    public static JSONObject getChapterById(String  id) {
        DatabaseConnection db= new DatabaseConnection();
        JSONObject res = null;
        Connection conn = db.getConnection();
        PreparedStatement stmt = null;
        try {
            String sql1 = "SELECT * FROM Tbl_chapter_page where fld_chapter_id=?";
            System.out.println(sql1);
            stmt = conn.prepareStatement(sql1);
            stmt.setString(1, id);
            ResultSet rs1 = stmt.executeQuery();
            JSONArray bookArray = new JSONArray();
            while(rs1.next()){
                JSONObject tmp=new JSONObject();
                tmp.put("pageId", rs1.getString("fld_page_id"));
                tmp.put("pageNum", rs1.getString("fld_page_number"));
                tmp.put("content", rs1.getString("fld_page_content"));
                bookArray.put(tmp);
            }
            res = new JSONObject();
            res.put("success", true);
            res.put("message", "featch ch successfully");
            res.put("data", bookArray);
            stmt.close();
        } catch (Exception e1) {
            e1.printStackTrace();
            res = new JSONObject();
            res.put("success", false);
            res.put("message", " something went wrong");
        }
        return res;
    }
}
