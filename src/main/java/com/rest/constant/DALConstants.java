package com.rest.constant;

/**
 * Created by root on 15/11/15.
 */
public class DALConstants {
    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static final String host = "127.0.0.1";
    public static final String port = "3306";
    public static final String DB_URL = "jdbc:mysql://"+host+":"+port+"/bookstore";
    public static final String USER = "root";
    public static final String PASS = "root";
}
