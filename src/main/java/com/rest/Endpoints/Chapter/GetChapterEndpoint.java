package com.rest.Endpoints.Chapter;

import org.json.JSONObject;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static com.rest.Database.GetChapter.getChapterById;
import static com.rest.Database.Getbook.getBookById;

/**
 * Created by pavan on 1/3/16.
 */
public class GetChapterEndpoint extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //Run.start();
        System.out.println(request.getRequestURI().substring(request.getContextPath().length()));
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        //UserLogin pc = UserLoginMapper.map(request);
        //System.out.println(pc);
        System.out.println("paka");
        System.out.println(request.getParameter("id"));
        //System.out.println(pc.email);
        String id=request.getParameter("id");
        JSONObject res=  getChapterById(id);
        out.write(res.toString());
        //Run.end();
    }
}
